# Matlab Engine Examples

Matlab Engine is an officially supported python package that enables python code to execute matlab code.

This project provide a quick example of using the package (see `matlabengine-example.py`).


More information about the package is avaialble at the [official project site](https://www.mathworks.com/help/matlab/matlab-engine-for-python.html)


# Run this example

Execute this command from the project root:

`python matlabengine-example.py`

*note that matlab must be installed on the host computer for the `matlabengine` package to function correctly*