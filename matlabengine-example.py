# Package installed with the command `pip install matlabengine` from command line.
# However, note that you must also have matlab installed for this code to work correctly.
#
# More details here: https://www.mathworks.com/help/matlab/matlab_external/install-the-matlab-engine-for-python.html

import matlab
import matlab.engine

import numpy as np


# To get started, we create an instance of the matlab computation engine.
# This is conceptually a programmatic matlab "command window" - we will
# invoke matlab commands through this engine object.
matlab_engine = matlab.engine.start_matlab()



# Builtin matlab functions can be called directly through this engine object.
# However, note that - although we can hand in scaler value directly - we need to
# do a little bit of manipulation to multi-dimensional array / matricies (see below).
x = 37
result = matlab_engine.isprime(x) # `isprime` is a builtin matlab function
print(f'According to matlab, is {x} prime? {result}')
print()



# We can also call custom / user-defined functions.
#
# To do this, make sure the engine's "working directory" is in the correct location (i.e.,
# the folder where our matlab code is stored)...
matlab_engine.cd(r'./matlab_code')

# ...then, call the user-defined function with the same syntax from the previous example.
vals = np.array([[1,2,3], [4,5,6]], dtype='float')
squared_vals = matlab_engine.square_matrix_elements(matlab.double(vals))
print(f'Computation executed in matlab: \nInitial Values:\n{vals} \nSquared Values:\n{squared_vals}')
print()



# It is common for matlab functions to have multiple return values.
# By default, the matlab_engine only returns the first return value.
# We can access additional values with the `nargout` keyword argument.
# 
# For example, my user-defined `divide_with_remainder` function returns two values -
# the `nargout=2` parameter explicitly tells the engine to include the second return value as well.
x, y = 5, 3
(result, remainder) = matlab_engine.divide_with_remainder(x, y, nargout=2)
print(f'{x} divided by {y} is {result} with a remainder of {remainder}')



# One slight inconsistency arises when working with uni-dimensional python arrays.
# Matlab seems to up-convert everything to a matrix (i.e., >= 2 dimensions)
#
# Note the inconsist data-structure dimension in the example below:
#   Before matlab - One-dimensional array
#   After matlab - Two-dimensional (3x1) matrix
one_dimensional_array = np.array([1,2,3], dtype='int64')
two_dimensional_matrix = matlab.int64(one_dimensional_array)
two_dimensional_result = matlab_engine.square_matrix_elements(two_dimensional_matrix)
print(f'matlab seems to up-convert one-dimensional python arrays (e.g., {one_dimensional_array}) into multi-dimensional matricies (e.g., {two_dimensional_result})')
print()



# Another notable point - the values returned by matlab_engine computation are a package specific datatype, and are *not* generic python.numpy arrays.
# Conversion back and forth may occur implictly, however you can explicitly covert as follows:

matlab_matrix = matlab.double([[1,2,3], [4,5,6]])
numpy_array = np.array(matlab_matrix)

### Note that `matlab.double` (and the whole family of matrix constructors) expects a to be handed a compatible datatype.
### This raises an error if I hand in a numpy array of *integers*
another_numpy_array = np.array([[7,8,9], [10,11,12]], dtype='float')
another_matlab_matrix = matlab.double(another_numpy_array)