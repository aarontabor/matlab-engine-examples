function [x_squared] = square_matrix_elements(x)
    x_squared = x .* x;
end

