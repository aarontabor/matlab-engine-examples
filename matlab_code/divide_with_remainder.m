function [result, remainder] = divide_with_remainder(x, y)
    result = floor(x/y);
    remainder = mod(x,y);
end

